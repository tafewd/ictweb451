-- Example of SQL comment

/* Example of SQL comment*/

SELECT 1+2 AS 'Addition', 1 AS 'Number', 1-9 AS 'Substruction', 3*5, 4/0, 21%5, 22%5, 25%5, 5*4;

-- This is modular division
SELECT 17%6 AS 'Mod', 17/6, (17/6)*6, 17-((17/6)*6);

-- This is modular division
/* This is
multiline comment */
SELECT 17%6 AS 'Mod', 17/6, /*(17/6)*6,*/ 17-((17/6)*6);

SELECT COUNT(SupplierID) AS 'No of suppliers' FROM [Suppliers]

-- All suppliers from UK or Australia that have 'St' in their address
SELECT * FROM [Suppliers] WHERE (Country = 'Australia' OR Country = 'UK') AND Address LIKE 'St%';

-- All suppliers from UK or Australia that have '555' in their phone number
SELECT * FROM [Suppliers] WHERE (Country = 'Australia' OR Country = 'UK') AND Phone LIKE '%555%' ;

-- All suppliers from UK or Australia that have '555' in their phone number
SELECT * FROM [Suppliers] 
WHERE NOT (Country = 'Australia' OR Country = 'UK') AND Phone LIKE '%555%' 
ORDER BY City DESC;

-- Update comment
SELECT * FROM [Suppliers] 
WHERE NOT (Country = 'Australia' OR Country = 'UK') AND SupplierID >= 10 AND SupplierID <= 20
ORDER BY City DESC;

-- Display number of suppliers from Japan with ID > = 20
SELECT * FROM [Suppliers]
WHERE (Country = 'Japan' OR Country = 'USA') 

-- Display number of suppliers from Japan and US
SELECT * FROM [Suppliers]
WHERE Country IN ('Japan', 'USA', "UK", 'Australia') 

-- Display number of suppliers from Japan and US
SELECT * FROM Suppliers WHERE Phone LIKE "%555%" ORDER BY City DESC;
