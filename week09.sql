-- Display the contact name, address, city and country of all customer which are located in 
--   Mexico, UK and Italy. Use the “in” Operator for Language.
SELECT ContactName as Name, Address, City, Country FROM `customers`
WHERE Country IN ('Mexico', 'UK', 'Italy')
ORDER BY Country;

SELECT ContactName as Name, Address, City, Country FROM `customers`
WHERE Country = 'Mexico' OR Country = 'UK' OR Country = 'Italy'
ORDER BY Country;

-- Display the employee's first name, last name and age. 
--   The age is a calculated field. Hint: use CURDATE and YEAR functions.
SELECT CURDATE() AS 'Current Date';
SELECT YEAR(CURDATE()) AS 'Current Year';

SELECT FirstName, LastName, BirthDate, YEAR(BirthDate) AS 'Birth Year', YEAR(CURDATE()) AS 'Current Year', 
YEAR(CURDATE()) - YEAR(BirthDate) AS 'Age'
FROM `employees`;

-- Display product name and price for all products in Beverages, Condiments and Confections categories 
--   which suppliers are not located in the USA.
SELECT ProductName, Price FROM `products`;

SELECT ProductName, Price FROM `products`
INNER JOIN `suppliers` ON `suppliers`.`SupplierID` = `products`.`SupplierID`
INNER JOIN `categories` ON `categories`.`CategoryID` = `products`.`CategoryID`;

SELECT ProductName, Price, CategoryName  FROM `products`
INNER JOIN `suppliers` ON `suppliers`.`SupplierID` = `products`.`SupplierID`
INNER JOIN `categories` ON `categories`.`CategoryID` = `products`.`CategoryID`
WHERE `categories`.`CategoryName` IN ('Beverages', 'Condiments', 'Confections')
;

SELECT ProductName, Price, CategoryName, Country  FROM `products`
INNER JOIN `suppliers` ON `suppliers`.`SupplierID` = `products`.`SupplierID`
INNER JOIN `categories` ON `categories`.`CategoryID` = `products`.`CategoryID`
WHERE `categories`.`CategoryName` IN ('Beverages', 'Condiments', 'Confections')
ORDER BY Country;

SELECT ProductName, Price, CategoryName, Country  FROM `products`
INNER JOIN `suppliers` ON `suppliers`.`SupplierID` = `products`.`SupplierID`
INNER JOIN `categories` ON `categories`.`CategoryID` = `products`.`CategoryID`
WHERE `categories`.`CategoryName` IN ('Beverages', 'Condiments', 'Confections') 
 AND `suppliers`.`Country` != 'USA'
ORDER BY Country;

-- Find out which country has the most customers. Hint: use GROUP BY and COUNT().

-- returns number of customers.
SELECT COUNT(Country) FROM `customers`  
ORDER BY `customers`.`Country` ASC;

-- list of countries
SELECT DISTINCT Country FROM `customers`  
ORDER BY `customers`.`Country` ASC;

-- count of distinct countries
SELECT COUNT(DISTINCT Country) FROM `customers`  
ORDER BY `customers`.`Country` ASC;

SELECT Country, COUNT(CustomerID) AS "Count" FROM `customers`  
GROUP BY Country
ORDER BY COUNT(CustomerID) DESC;
