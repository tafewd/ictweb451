-- Use https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all
-- Database: https://github.com/AndrejPHP/w3schools-database/blob/master/w3schools.sql

SELECT * FROM Employees WHERE FirstName = "Nancy";

SELECT * FROM Employees WHERE EmployeeID = 2 OR EmployeeID = 3 ;

SELECT * FROM Employees WHERE EmployeeID > 1 OR EmployeeID < 4;

SELECT * FROM [Suppliers] ORDER BY ContactName ASC;
