-- Tasks: week 08
-- Import database from `week08-w3schools.sql` database.

-- Create the following queries.

-- Task 1.a: Display the last supplier name after sorting suppliers in descending alphabetical order.
SELECT * FROM `suppliers` ORDER BY `suppliers`.`SupplierName` DESC LIMIT 1;

-- Task 1.b: Display category names.
SELECT CategoryName FROM `categories` ORDER BY `categories`.`CategoryName` ASC;

-- Task 1.c: How many orders are there?

-- Task 1.d: Display all supplier's country and city. Make sure there are no duplicates.
SELECT DISTINCT City, Country FROM `customers` ORDER BY `customers`.`City` ASC;
SELECT DISTINCT(City), Country FROM `customers` ORDER BY `customers`.`City` ASC;

-- Task 2.a: Display orders dates for orders that were ordered from Mexico.
SELECT * FROM `orders` 
INNER JOIN `customers` ON `orders`.`CustomerID`=`customers`.`CustomerID` 
WHERE `Country`="Mexico" 
ORDER BY `orders`.`CustomerID` ASC;

-- Task 2.b: Display all orders by empoloyee with name Steven Buchanan 
SELECT FirstName, LastName, OrderID FROM orders 
INNER JOIN employees ON employees.EmployeeID AND Orders.EmployeeID 
WHERE FirstName="Steven" AND LastName="Buchanan" 
ORDER BY OrderID ASC;

-- Task 2.c: Display all products from beverages and condiments category.
SELECT * FROM `products` 
INNER JOIN `categories` ON products.CategoryID = categories.CategoryID 
WHERE CategoryName = "Beverages" OR CategoryName = "Condiments";

-- Task 2.d: Display all orders from Finland that were shipped by Federal Shipping.
-- How many orders? SELECT COUNT(*)...
-- How many unique customers? SELECT DISTINCT CustomerId
SELECT * FROM `orders` 
INNER JOIN `customers` ON `orders`.`CustomerID`=`customers`.`CustomerID` 
INNER JOIN `shippers` ON `orders`.`ShipperID`=`shippers`.`ShipperID` 
WHERE `Country`="Finland" AND `ShipperName` LIKE "Federal%"
ORDER BY `orders`.`CustomerID` ASC;

